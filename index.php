<!doctype>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Capitán Helado Delivery">
    <meta name="theme-color" content="#383267">
    <link rel="icon" href="img/fav.ico">
    
    <title>Capitán Helado</title>
	
	<!-- Css -->
    <link rel="stylesheet" type="text/css" href="css/normalize.5.0.0.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	
	<header>
		<div class="container">
			<h1>
				<span>¿Pedimos</span>Helado?
			</h1>
		</div>
	</header>

	<section>
		<div class="container">
			<div class="logo-mobile">
				<img src="img/isologonegativo.svg" alt="Capitán Helado Delivery">
			</div>
			
			<div class="cont-mobile">
				<a href="tel://03414256134" class="telefono">4256134</a>
				<p class="horarios"><span>Domingos</span> a <span>Miércoles</span> de 20 a 00:30hs.</p>
				<p class="horarios"><span>Jueves</span> a <span>Sábados</span> de 20 a 01:30hs.</p>
			</div>
			
			<div class="col-1-3 pedidos">
				<p class="pedi">¡Pedí 2 cuartos por $120!</p>
				<img class="potes" src="img/potes.png" alt="Elegí tu pote">
			</div>
			
			<div class="col-1-3 proximamente">
				<img class="logo" src="img/logo.svg" alt="Capitán Helado Delivery">
				
				<div class="prox">
					<p>Próximamente</p>
					<img  class="" src="img/android.png" alt="Andriod APP">
					<img  class="" src="img/apple.png" alt="Apple APP">
					<img  class="" src="img/windows.png" alt="Windows Phnoe APP">
				</div>
			</div>
			
			<div class="col-1-3 smart">
				<img class="smart" src="img/celular.png" alt="Capitán Helado App">
			</div>
		</div>
	</section>
	
	<footer>
			<div class="footer-top"></div>
			<div class="container">
				<div class="col-1-3">
					<img class="logo-footer" src="img/isologo.svg" alt="Capitán Helado Delivery">
				</div>
				<div class="col-1-3">
					<a class="telefono">4256134</a>
					<p class="horarios"><span>Domingos</span> a <span>Miércoles</span> de 20 a 00:30hs.</p>
					<p class="horarios"><span>Jueves</span> a <span>Sábados</span> de 20 a 01:30hs.</p>
				</div>
				<div class="col-1-3">
					<div class="envios col-1-2">
						<div class="zona-mobile">
							<img src="img/envio-incluido.png" alt="Envìo Incluìdo">
							<p>27 DE FEBRERO</p>
							<p>AV. FRANCIA</p>
						</div>
					</div>
					<div class="col-1-2">
						<div class="gustos" onclick="mostrarGustos();">
							NUESTROS<br>GUSTOS
						</div>
						<div class="redes">
							<p>¡Seguinos!</p>
							<a href="https://www.facebook.com/capitanhelado.rosario/" target="_blank">
								<img src="img/fb.png" alt="Facebook FanPage">
							</a>
							<a href="https://www.instagram.com/capitanhelado/" target="_blank">
								<img src="img/ins.png" alt="Instagram">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
	</footer>

	<div class="popup">
		<img class="close" src="img/close.svg" onclick="ocultarGustos();">
		
		<div class="container">
			<div class="col-1-2">
				<img src="img/gustos/01.jpg" alt="CREMA AMERICANA">
				<div class="desc">
					<p class="tit">CREMA AMERICANA</p>
					<p class="bajada">Crema helada sabor chantilly.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/02.jpg" alt="CREMA CON COOKIES">
				<div class="desc">
					<p class="tit">CREMA CON COOKIES</p>
					<p class="bajada">Crema helada sabor natural con trocitos de cookies de chocolate.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/03.jpg" alt="CREMA CON CONFITES">
				<div class="desc">
					<p class="tit">CREMA CON CONFITES</p>
					<p class="bajada">Crema helada sabor natural con coloridos confites de chocolate.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/04.jpg" alt="CREMA GRANIZADA">
				<div class="desc">
					<p class="tit">CREMA GRANIZADA</p>
					<p class="bajada">Crema helada sabor natural granizada con chocolate Stracciatella.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/05.jpg" alt="CREMA TRAMONTANA">
				<div class="desc">
					<p class="tit">CREMA TRAMONTANA</p>
					<p class="bajada">Crema helada sabor natural con microgalletitas bañadas en chocolate y dulce de leche familiar.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/06.jpg" alt="CHOCOLATE NATURAL">
				<div class="desc">
					<p class="tit">CHOCOLATE NATURAL</p>
					<p class="bajada">Crema helada de chocolate natural.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/07.jpg" alt="CHOCOLATE CAPITÁN">
				<div class="desc">
					<p class="tit">CHOCOLATE CAPITÁN</p>
					<p class="bajada">Crema helada de chocolate natural con trozos de nueces, crocante de maní y granizado con chocolate Stracciatella.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/08.jpg" alt="CHOCOLATE SÚPER">
				<div class="desc">
					<p class="tit">CHOCOLATE SÚPER</p>
					<p class="bajada">Crema helada de chocolate natural y exquisito dulce de leche familiar.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/09.jpg" alt="CHOCOLATE CON ALMENDRAS">
				<div class="desc">
					<p class="tit">CHOCOLATE CON ALMENDRAS</p>
					<p class="bajada">Crema helada de chocolate natural con almendras especiales partidas.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/10.jpg" alt="CHOCOLATE CON CAFÉ">
				<div class="desc">
					<p class="tit">CHOCOLATE CON CAFÉ</p>
					<p class="bajada">Crema helada de chocolate natural combinada con una crema especial de dulce de leche y café intenso.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/11.jpg" alt="DULCE DE LECHE">
				<div class="desc">
					<p class="tit">DULCE DE LECHE</p>
					<p class="bajada">Crema helada de exquisito dulce de leche.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/12.jpg" alt="DULCE DE LECHE CHOCOTORTA">
				<div class="desc">
					<p class="tit">DULCE DE LECHE CHOCOTORTA</p>
					<p class="bajada">Crema helada de dulce de leche con trozos de galletas de chocolate y dulce de leche familiar.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/13.jpg" alt="DULCE DE LECHE GRANIZADO">
				<div class="desc">
					<p class="tit">DULCE DE LECHE GRANIZADO</p>
					<p class="bajada">Crema helada de dulce de leche granizada con chocolate Stracciatella.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/14.jpg" alt="DULCE DE LECHE TENTACIÓN">
				<div class="desc">
					<p class="tit">DULCE DE LECHE TENTACIÓN</p>
					<p class="bajada">Crema helada de dulce de leche con microgalletitas bañadas en chocolate y dulce de leche familiar.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/15.jpg" alt="VAINILLA">
				<div class="desc">
					<p class="tit">VAINILLA</p>
					<p class="bajada">Crema helada sabor vainilla.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/16.jpg" alt="FLAN DE VAINILLA">
				<div class="desc">
					<p class="tit">FLAN DE VAINILLA</p>
					<p class="bajada">Crema helada sabor vainilla con una combinación de dulce de leche familiar y caramelo.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/17.jpg" alt="CREMA BORRACHA">
				<div class="desc">
					<p class="tit">CREMA BORRACHA</p>
					<p class="bajada">Crema helada sabor vainilla con masitas vainillas embebidas en vino dulce Marsala.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/18.jpg" alt="CREMA DE AVELLANAS">
				<div class="desc">
					<p class="tit">CREMA DE AVELLANAS</p>
					<p class="bajada">Crema helada de un distintivo sabor a avellanas y cacao con crema de chocolate y crocante de maní.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/19.jpg" alt="CREMA BOMBÓN">
				<div class="desc">
					<p class="tit">CREMA BOMBÓN</p>
					<p class="bajada">Crema helada de maní sembrada con una deliciosa crema de bombón crujiente.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/20.jpg" alt="CREMA DE NUECES">
				<div class="desc">
					<p class="tit">CREMA DE NUECES</p>
					<p class="bajada">Crema helada de un intenso y natural sabor a nueces sembrada con trozos de nueces.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/21.jpg" alt="MENTA GRANIZADA">
				<div class="desc">
					<p class="tit">MENTA GRANIZADA</p>
					<p class="bajada">Crema helada de un intenso sabor a menta granizada con chocolate Stracciatella.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/22.jpg" alt="SAMBAYÓN">
				<div class="desc">
					<p class="tit">SAMBAYÓN</p>
					<p class="bajada">Crema helada a base de huevo, azúcar y vino Marsala de consistencia cremosa y ligera de sabor muy particular.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/23.jpg" alt="TIRAMISÚ">
				<div class="desc">
					<p class="tit">TIRAMISÚ</p>
					<p class="bajada">Crema helada de delicado sabor café obtenida de una cuidadosa combinación de crema batida con azúcar, café, cacao y yemas de huevos.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/24.jpg" alt="BANANA SPLIT">
				<div class="desc">
					<p class="tit">BANANA SPLIT</p>
					<p class="bajada">Crema helada de bananas naturales con dulce de leche familiar.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/25.jpg" alt="CREMA DE FRUTILLAS">
				<div class="desc">
					<p class="tit">CREMA DE FRUTILLAS</p>
					<p class="bajada">Crema helada de frutillas naturales.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/26.jpg" alt="CREMA DE QUINOTOS">
				<div class="desc">
					<p class="tit">CREMA DE QUINOTOS</p>
					<p class="bajada">Crema helada de quinotos naturales.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/27.jpg" alt="ANANÁ AL AGUA">
				<div class="desc">
					<p class="tit">ANANÁ AL AGUA</p>
					<p class="bajada">Helado al agua sabor ananá con trocitos de ananá en almíbar.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/28.jpg" alt="DURAZNO AL AGUA">
				<div class="desc">
					<p class="tit">DURAZNO AL AGUA</p>
					<p class="bajada">Helado al agua sabor durazno con trocitos de durazno en almíbar.</p>
				</div>
			</div>
			<div class="col-1-2">
				<img src="img/gustos/29.jpg" alt="LIMÓN AL AGUA">
				<div class="desc">
					<p class="tit">LIMÓN AL AGUA</p>
					<p class="bajada">Helado al agua hecho con limones naturales liofilizados.</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<div id="preload">
		<div class="bar"></div>
	</div>

	<!-- JS -->
	<script src="js/jquery.3.2.1.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>