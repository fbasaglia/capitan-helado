function mostrarGustos(){
	$( 'body' ).toggleClass( 'noscroll' );
	$( '.popup' ).toggleClass( 'block' );

	setTimeout( function() { 
		$( '.popup' ).toggleClass( 'active' );
	}, 1 );
}

function ocultarGustos(){
   	$( 'body' ).toggleClass( 'noscroll' );
	$( '.popup' ).toggleClass( 'active' );
	
	setTimeout( function() {
		$( '.popup' ).toggleClass( 'block' );
	}, 300 );
}

$( document ).ready( function() {

	setTimeout( function() {
		$( '#preload' ).fadeOut( 200 );
	}, 900 );

});